from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse
from models.contacts_model import AddUserInContact_list
from services import user_service
from services import contact_service
from models.user_model import UserCredentials


contacts_router = APIRouter(prefix='/contacts')


@contacts_router.post('/from_all_users', tags=["Private part"])
def add_friend_to_contact_list_from_all_users(new_friend: AddUserInContact_list,
                               current_user: UserCredentials = Depends(user_service.get_current_user)):
    if current_user is None:
        return JSONResponse(status_code=404, content='User not found!')
    user_id = current_user.id
    friend_name = new_friend.username
    all_usernames = contact_service.get_all_usernames()
    if friend_name not in all_usernames:
        return JSONResponse(status_code=404, content=f'User with username {friend_name} not exist!')
    my_contact_ids = contact_service.get_all_contact_ids_for_auth_user(user_id)
    my_contacts_usernames = contact_service.get_all_usernames_from_user_ids(my_contact_ids)
    if new_friend.username in my_contacts_usernames:
        return JSONResponse(status_code=400, content=f"User with username {friend_name} is already in your contact list!")
    contact_service.add_friend_in_contacts(user_id, new_friend)
    return JSONResponse(status_code=200, content=f'User with username {friend_name} added in to your contact list!')
    


@contacts_router.post('/from_transactions', tags=["Private part"])
def add_friend_to_contact_list_from_transactions(new_friend: AddUserInContact_list,
                                                 current_user: UserCredentials = Depends(user_service.get_current_user)):
    if current_user is None:
        return JSONResponse(status_code=404, content='User not found!')
    user_id = current_user.id
    friend_name = new_friend.username
    transaction_ids = contact_service.get_all_reciever_ids_for_current_user(user_id)
    transaction_usernames = contact_service.get_all_usernames_from_user_ids(transaction_ids)
    if friend_name not in transaction_usernames:
        return JSONResponse(status_code=404, content=f'User with username {friend_name} not in transaction list')
    friend_ids = contact_service.get_all_contact_ids_for_auth_user(user_id)
    my_usernames = contact_service.get_all_usernames_from_user_ids(friend_ids)
    if friend_name in my_usernames:
        return JSONResponse(status_code=400, content=f"User with username {friend_name} is already in your contact list!")
    contact_service.add_friend_in_contacts(user_id, new_friend)
    return JSONResponse(status_code=200, content=f'User with username {friend_name} added in to your contact list!')



@contacts_router.delete('/{username}', tags=["Private part"])
def delete_user_from_contact_list(username: str,
                                  current_user: UserCredentials = Depends(user_service.get_current_user)):
    if current_user is None:
        return JSONResponse(status_code=404, content='User not found!')   
    else:
        user_id = current_user.id
        existing_ids = contact_service.get_all_contact_ids_for_auth_user(user_id)
        existing_usernames = contact_service.get_all_usernames_from_user_ids(existing_ids)
        friend_id = contact_service.get_friend_id(username)
        if username not in existing_usernames:
            return JSONResponse(status_code=404, content=f'User with username {username} not in your contact list!')
        else:
            contact_service.delete_username_from_contact_list(friend_id)
            return JSONResponse(status_code=200, content=f'User {username} succesfully deleted from your contact list!')



@contacts_router.get('/all_friends', tags=["Private part"])
def get_all_friends_for_auth_user(
    current_user: UserCredentials = Depends(user_service.get_current_user)):
    if current_user is None:
        return JSONResponse(status_code=404, content='User not found!')
    user_id = current_user.id
    friend_ids = contact_service.get_all_contact_ids_for_auth_user(user_id)
    usernames = contact_service.get_all_usernames_from_user_ids(friend_ids)
    return usernames