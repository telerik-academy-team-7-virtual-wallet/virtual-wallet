from fastapi import APIRouter, HTTPException, Depends
from fastapi.responses import JSONResponse
from models.wallet_model import Wallets, CreateWallet
from services import wallet_service
from services import user_service
from models.user_model import UserCredentials


wallets_router = APIRouter(prefix='/wallets')


@wallets_router.get('/{id}', tags=["Private part"])
def get_wallet_by_id(id: str, current_user: UserCredentials = Depends(user_service.get_current_user)):
    if current_user is None:
        return JSONResponse(status_code=404, content='User not found!')
    if len(id) != 36:
        return JSONResponse(status_code=400, content='ID must be exacrly 36 characters!')
    else:
        existing_wallet = wallet_service.get_by_id(id)
        if existing_wallet is None:
            return JSONResponse(status_code=404, content='Wallet with this id not exists!')
        else:
            return existing_wallet


@wallets_router.get('/', tags=["Private part"])
def get_all_wallets_for_auth_user(current_user: UserCredentials = Depends(user_service.get_current_user)):
    if current_user is None:
        return JSONResponse(status_code=404, content='User not found!')
    else:
        user_id = current_user.id
        return wallet_service.get_all_wallets_for_auth_user(user_id)
    

@wallets_router.post('/', status_code=201, tags=["Private part"])
def create_wallet(wallet: CreateWallet=Depends(), current_user: UserCredentials = Depends(user_service.get_current_user)):
    if current_user is None: 
        return JSONResponse(status_code=404, content='User not found!')
    else:
        if len(current_user.id) != 36:
            return JSONResponse(status_code=400, content='ID must be exacrly 36 characters!')
        if len(wallet.wallet_name) < 3 or len(wallet.wallet_name) > 45:
            return JSONResponse(status_code=400, content='Wallet name can be between 3 and 45 characters!')
        admin_id = current_user.id
        new_wallet = wallet_service.create_wallet(wallet, admin_id)
        return new_wallet
    


@wallets_router.delete('/{id}', tags=["Private part"])
def delete_wallet(id: str, current_user: UserCredentials = Depends(user_service.get_current_user)):
    if current_user is None:
        return JSONResponse(status_code=404, content='User not found!')
    else:
        if len(id) != 36:
            return JSONResponse(status_code=404, content='Id must be exactly 32 characters!')
        user_id = current_user.id
        wallet_ids = wallet_service.get_all_wallet_ids_for_auth_user(user_id)
        if id in wallet_ids:
            wallet_service.delete_wallet(id)
            return JSONResponse(status_code=200, content='Wallet Deleted!')
        else:
            raise HTTPException(status_code=404, detail='Wallet with this id not in your wallet ids!')






    

