from typing import Literal
from fastapi import APIRouter, Depends, HTTPException, Path
from fastapi.security import OAuth2PasswordRequestForm
from fastapi.responses import JSONResponse, HTMLResponse
from models import user_model, bank_card_model
from services import user_service, bank_card_service
from utils.jwt_token import create_token
from utils.password_hasher import check_password
from fastapi_pagination import Page, paginate

users_router = APIRouter(prefix="/users")


# GET / "users/me/bank_cards/"  - return all back cards to the users
# POST / "users/me/bank_cards/" - create new bank card
# GET / "users/me/wallets/" - return all created wallets to the users
# POST / "users/me/wallets/" - create new wallet


@users_router.post("/", tags=["Public part"])
def create_user(new_user: user_model.UserRegister):
    return user_service.create_new_user(new_user)


@users_router.get("/{username}/confirm", tags=["Public part"])
def get_confirm_email(username: str):
    r = confirm_email(username=username)
    if r.status_code == 200:
        return HTMLResponse(f"<body>Email of user {username} confirmed.</body>")
    
    if r.status_code == 404:
        body = r.body
        body = body.decode(encoding="utf-8")
        return HTMLResponse(f"<h1>404</h1><h3> {body} </h3>")

@users_router.patch("/{username}/confirm", tags=["Public part"])
def confirm_email(username: str):
    return user_service.confirm_email(username=username)

@users_router.post("/token", tags=["Public part", "Private part"])
def login(form_data: OAuth2PasswordRequestForm = Depends()):
    current_user = user_service.get_user_by_username(form_data.username)
    if not current_user:
        raise HTTPException(status_code=400, detail="Incorrect username or password")
    if current_user.is_inactive:
        raise HTTPException(status_code=400, detail="The user has been deactivated.")
    if not check_password(form_data.password, current_user.password):
        raise HTTPException(status_code=400, detail="Incorrect username or password")

    return user_service.access_token(current_user)


@users_router.get("/me", tags=["Private part"])
def get_logged_user_info(current_user: user_model.UserCredentials = Depends(user_service.get_current_user)):
    return user_service.get_user_by_id(current_user.id)


@users_router.patch("/me", tags=["Private part"])
def update_logged_user_info(new_credentials: user_model.UserInfoUpdate,
                            current_user: user_model.UserCredentials = Depends(user_service.get_current_user)):
    if new_credentials.new_password:
        if not check_password(new_credentials.old_password, user_service.get_logged_user_password(current_user.id)):
            raise HTTPException(status_code=400, detail="Incorrect password.")

    return user_service.update_logged_user(current_user.id, new_credentials)

@users_router.patch("/me/deactivate", tags=["Private part"])
def delete_logged_user(current_user: user_model.UserCredentials = Depends(user_service.get_current_user)):
    return user_service.deactivate(current_user.id)

@users_router.get("/", tags=["Admin part"], dependencies=[Depends(user_service.get_current_user)])
def get_users(search_by: Literal["username", "email", "phone"] | None = None,
              value: str | None = None,
               l_user: user_model.UserCredentials = Depends(user_service.get_current_user)) -> Page[user_model.UserInfo]:
    if l_user.role != "admin":
        raise HTTPException(status_code=403, detail="You are not authorized. Admins Only.")

    if (search_by != None and value == None) or (search_by == None and value != None):
        raise HTTPException(status_code= 400,
                            detail="Search_by and value queries must be used together. One of the queries mising.")
    if not search_by:
        return paginate(user_service.get_all_users())

    return paginate(user_service.get_user(seach_by=search_by, val=value))



@users_router.get("/{id}", tags=["Admin part"])
def get_user_by_id(id: str,
                   l_user: user_model.UserCredentials = Depends(user_service.get_current_user)):
    if l_user.role != "admin":
        raise HTTPException(status_code=403, detail="You are not authorized. Admins Only.")

    return user_service.get_user_by_id(id)

@users_router.patch("/{user_id}/access/{status}}", tags=["Admin part"])
def block_or_unblock_user(user_id: str,
                          status: Literal["block", "unblock"] = Path(...),
                          l_user: user_model.UserCredentials = Depends(user_service.get_current_user)):

    if l_user.role != "admin":
        raise HTTPException(status_code=403, detail="You are not authorized. Admins Only.")
    
    return user_service.block_unblock_user_by_id(user_id, status)

@users_router.post('/me/bank_cards', status_code=201, tags=['Private part'])
def create_bank_card(card: bank_card_model.BankCardAdd,
                     current_user: user_model.UserCredentials = Depends(user_service.get_current_user)):
    return bank_card_service.create(card, current_user)


@users_router.get('/me/bank_cards', tags=['Private part'])
def get_user_cards(current_user: user_model.UserCredentials = Depends(user_service.get_current_user)):
    return bank_card_service.get_cards_by_user(current_user)


@users_router.delete('/me/bank_cards/{bank_card_id}', tags=['Private part'])
def remove_card(card_id, current_user: user_model.UserCredentials = Depends(user_service.get_current_user)):

    if len(card_id) != 36:
        return JSONResponse(status_code=422, content='Id must be exactly 32 characters!')

    if not bank_card_service.exists(card_id):
        return JSONResponse(status_code=404, content='Bank card not found')

    return bank_card_service.delete(card_id, current_user)


# ceco5
# Aa1234!@#$

# admin
# admin
