from fastapi import APIRouter, Depends, HTTPException
from models import transaction_model, user_model
from services import user_service, transaction_service
from typing import Literal

transactions_router = APIRouter(prefix='/transactions')


# ToDo: make search optional when contacts added
@transactions_router.post('/', status_code=201, tags=['Private part'])
def create_transaction(transaction: transaction_model.CreateTransaction,
                       search_by: Literal["username", "email", "phone"],
                       val: str,
                       current_user: user_model.UserCredentials = Depends(user_service.get_current_user)):
    return transaction_service.prepare_transaction(transaction, current_user, search_by, val)


@transactions_router.put('/{txn_id}/confirm', tags=['Private part'])
def confirm_transaction(txn_id, current_user: user_model.UserCredentials = Depends(user_service.get_current_user)):
    return transaction_service.confirm(txn_id, current_user)


@transactions_router.put('{txn_id}/edit/', tags=['Private part'])
def edit_transaction(txn_id, transaction: transaction_model.UpdateTransaction,
                     current_user: user_model.UserCredentials = Depends(user_service.get_current_user),
                     search_by: Literal["username", "email", "phone"] = None, val: str = None):
    return transaction_service.update(txn_id, transaction, current_user, search_by, val)


@transactions_router.get('/pending', tags=['Private part'])
def get_pending_transactions(current_user: user_model.UserCredentials = Depends(user_service.get_current_user)):
    return transaction_service.get_pending_transactions_auth_user(current_user)


@transactions_router.patch('/pending/{txn_id}/accept', tags=['Private part'])
def accept_transaction(txn_id: str,
                       current_user: user_model.UserCredentials = Depends(user_service.get_current_user)):
    return transaction_service.accept_transaction(txn_id, current_user)


@transactions_router.patch('/pending/{txn_id}/decline', tags=['Private part'])
def decline_transaction(txn_id: str, current_user: user_model.UserCredentials = Depends(user_service.get_current_user)):
    return transaction_service.decline_transaction(txn_id, current_user)


@transactions_router.get('/all', tags=['Private part'])
def get_all_txn_for_auth_user(current_user: user_model.UserCredentials = Depends(user_service.get_current_user),
                              search_by: Literal["period", "recipient_id", "direction"] | None = None,
                              search_val: str | None = None, sort_by: Literal["amount", "time"] | None = None,
                              rev: bool = False):
    return transaction_service.get_auth_user_txns(current_user, search_by, search_val, sort_by, rev)


@transactions_router.get('/all/{user_id}', tags=['Admin part'])
def get_all_user_txn(user_id, current_user: user_model.UserCredentials = Depends(user_service.get_current_user),
                     search_by: Literal["period", "recipient_id", "direction"] | None = None,
                     search_val: str | None = None, sort_by: Literal["amount", "time"] | None = None,
                     rev: bool = False):

    if current_user.role != "admin":
        raise HTTPException(status_code=403, detail="You are not authorized. Admins Only.")

    return transaction_service.get_user_txns(user_id, search_by, search_val, sort_by, rev)
