from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse
from services import wallet_service
from services import user_service
from models.user_model import UserCredentials
from models.transfer_model import Transfer, Withdraw, TransferMoney



transfers_router = APIRouter(prefix='/transfers')



@transfers_router.put('/{wallet_id}/amount', tags=["Private part"])
def add_money_in_wallet(wallet_id: str, transfer: TransferMoney,
                        current_user: UserCredentials = Depends(user_service.get_current_user)):
    if current_user is None:
        return JSONResponse(status_code=404, content='User not found!')
    user_id = current_user.id    
    wallet_ids = wallet_service.get_all_wallet_ids_for_auth_user(user_id)
    if len(wallet_id) != 36:
        return JSONResponse(status_code=400, content='ID must be exactly 36 characters') 
    if wallet_id not in wallet_ids:    
        return JSONResponse(status_code=404, content="You don't have wallet with this id!")
    cards = wallet_service.get_all_bank_card_ids_for_auth_user(user_id)
    card_ids = [str(uuid) for uuid in cards]
    bank_card = transfer.bank_card
    if len(bank_card) != 36:
        return JSONResponse(status_code=404, content='Bank card must be exactly 36 characters!')    
    if bank_card not in card_ids:
        return JSONResponse(status_code=404, content="You don't have bank card with this id!")
    else:
        wallet_service.add_amount_in_wallet(user_id, wallet_id=wallet_id, changed_wallet=transfer)
        return JSONResponse(status_code=200, content=f'The sum: {transfer.amount} succesfully added in your wallet')




@transfers_router.put('/{wallet_id}/withdraw', tags=["Private part"])
def wallet_withdraw(id: str,
                    withdraw: Withdraw,
                    current_user: UserCredentials = Depends(user_service.get_current_user)):
    if current_user is None:
        return JSONResponse(status_code=404, content='User not found!')
    user_id = current_user.id
    wallet_ids = wallet_service.get_all_wallet_ids_for_auth_user(user_id)
    if len(id) != 36:
        return JSONResponse(status_code=400, content='ID must be exacrly 36 characters!')
    if id not in wallet_ids:    
        return JSONResponse(status_code=404, content="You don't have wallet with this id!")
    cards = wallet_service.get_all_bank_card_ids_for_auth_user(user_id)
    card_ids = [str(uuid) for uuid in cards]
    bank_card = withdraw.bank_card
    if len(bank_card) != 36:
        return JSONResponse(status_code=404, content='Bank card must be exactly 36 characters!')
    if bank_card not in card_ids:
        return JSONResponse(status_code=404, content="You don't have bank card with this id!")
    wallet_amount = wallet_service.get_wallet_amount_for_auth_user(user_id, id)
    if withdraw.amount > wallet_amount:
        return JSONResponse(status_code=400, content='The amount you want to withdraw is greater than the amount in your wallet')
    else:
        wallet_service.wallet_withdraw(user_id, wallet_id=id, changed_wallet=withdraw)
        return JSONResponse(status_code=200, content=f'The sum: {withdraw.amount} successfully withdrawn from your wallet')
    


@transfers_router.get('/', tags=["Private part"])
def get_all_transfers_for_user(
    current_user: UserCredentials = Depends(user_service.get_current_user)):
    if current_user is None:
        return JSONResponse(status_code=404, content='User not found!')
    else:
        user_id = current_user.id
        return wallet_service.get_all_transfers_for_auth_user(user_id)
    


@transfers_router.delete('/', tags=["Private part"])
def delete_all_transfers_for_user(
    current_user: UserCredentials = Depends(user_service.get_current_user)):
    if current_user is None:
        return JSONResponse(status_code=404, content='User not found!')
    else:
        user_id = current_user.id
        wallet_service.delete_all_transfers_for_auth_user(user_id)
        return JSONResponse(status_code=200, content=f'The transfers of a user {current_user.username} have been deleted')