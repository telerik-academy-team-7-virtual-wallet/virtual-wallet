from sqlmodel import Session, select, bindparam
from data.database import ENGINE
from models.user_model import UsersDB
from models.transaction_model import Transactions
from models.contacts_model import Contacts, AddUserInContact_list
from fastapi.responses import JSONResponse


def add_friend_in_contacts(user_id: str, new_friend: AddUserInContact_list):
    """
    Args:
        user_id (str):UUID Authenticated user
        new_friend (AddUserInContact_list): username
    Returns:
        Contacts model
    """
    with Session(ENGINE) as session:
        statement = select(UsersDB.id).where(UsersDB.username==new_friend.username)
        friend_id = session.exec(statement).first() 
        new_contact = Contacts(id=Contacts.id,
                           users_id=user_id,
                           users_id1=friend_id)
        session.add(new_contact)
        session.commit()
        session.refresh(new_contact)
        return new_contact


def get_all_usernames() -> list:
    """Return list of all usernames in database
    Returns:
        list: usernames
    """
    with Session(ENGINE) as session:
        statement = select(UsersDB.username)
        users = session.exec(statement).all()
        return users    


def get_all_contact_ids_for_auth_user(user_id: str) -> list:
    """Return a list of all contact ids for authenticated user
    Args:
        user_id (str):UUID Authenticated user
    Returns:
        list: Contacts.users_id1
    """
    with Session(ENGINE) as session:
        statement = select(Contacts.users_id1).where(Contacts.users_id==user_id)
        contacts = session.exec(statement).all()
        return contacts


def get_all_user_ids() -> list:
    """Return list of all User ids in database
    Returns:
        list: UserDB.id
    """
    with Session(ENGINE) as session:
        statement = select(UsersDB.id)
        users = session.exec(statement).all()
        return users 


def get_all_reciever_ids_for_current_user(user_id: str) -> list:
    """Return list of reciever ids from all transactions for current user
    Args:
        user_id (str):UUID Authenticated user
    Returns:
        list: Transactions.receiver_id
    """
    with Session(ENGINE) as session:
        statement = select(Transactions.receiver_id).where(Transactions.sender_id==user_id)
        recievers = session.exec(statement).all()
        return recievers
    
    
def get_all_usernames_from_user_ids(my_contact_ids: list) -> list:
    """Return list of all usernames in contact list for current user, using list of all contact list ids 
    Args:
        my_contact_ids (list): 
    Returns:
        list: UserDB.username for authenticated user
    """
    with Session(ENGINE) as session:
        statement = select(UsersDB.username).where(UsersDB.id.in_(bindparam('ids')))
        usernames = session.execute(statement, {"ids": my_contact_ids}).fetchall()
        return [username[0] for username in usernames]


def get_friend_id(username: str):
    """Using username the function return id for this username
    Args:
        username (str): 
    Returns:
        UserDB.id
    """
    with Session(ENGINE) as session:
        statement = select(UsersDB.id).where(UsersDB.username==username)
        user_id = session.exec(statement).first()
        return user_id


def delete_username_from_contact_list(id: str):
    """Delete username from contact list using UserDB.id
    Args:
        id (str): Contacts.users_id1
    """
    with Session(ENGINE) as session:
        session.query(Contacts).where(Contacts.users_id1==id).delete()
        session.commit()
        return JSONResponse(status_code=200, content='Contact deleted!')



