from sqlmodel import Session, select 
from models.category_model import Categories, UpdateCategory, CreateCategory
from data.database import ENGINE
from models.transaction_model import Transactions
from fastapi.responses import JSONResponse

def create_category(new_category: CreateCategory):
    """
    Creates a new category for the logged user.
    Args:
        new_category: CreateCategory model
    Returns:
        category
    """
    category = Categories(id=Categories.id,
                          category=new_category.category)
    with Session(ENGINE) as session:
        session.add(category)
        session.commit()
        session.refresh(category)
        return category
    


def delete_category_by_id(id: int):
    """Delete category by id
    Args:
        id (int): Categories.id
    """
    with Session(ENGINE) as session:
        session.query(Categories).filter_by(id=id).delete()
        session.commit()
        return JSONResponse(status_code=200, content='Category deleted!')



def get_category_by_id(id: int):
    """Return category searched by id
    Args:
        id (int): Categories.id

    Returns:
        Categories
    """
    with Session(ENGINE) as session:
        statement = select(Categories).where(Categories.id == id)
        category = session.exec(statement).first()
        return category
    

def update_category(id: int, new_category: UpdateCategory):
    """ 
    Args:
        id (int): Category.id
        new_category (UpdateCategory): category
    Returns:
        Categories
    """
    with Session(ENGINE) as session:
        category = session.query(Categories).filter(Categories.id == id).one()
        category.category = new_category.category
        session.commit()
        session.refresh(category)
        return category
    

def get_all_categories() -> list:
    """Return list of all categories
    Returns:
        list: Categories
    """
    with Session(ENGINE) as session:
        statement = select(Categories)
        categories = session.exec(statement).all()
        return categories


def get_all_category_names() -> list:
    """Return a list of all category names
    Returns:
        list: Categoies.category
    """
    with Session(ENGINE) as session:
        statement = select(Categories.category)
        categories = session.exec(statement).all()
        return categories


def get_category_by_name(name: str) -> Categories:
    """Return a category searched by name
    Args:
        name (str): 
    Returns:
        Categories: 
    """
    with Session(ENGINE) as session:
        statement = select(Categories).where(Categories.category == name)
        category = session.exec(statement).first()

    return category



def get_transactions_sum_for_category(category_id: int, user_id: str) -> list[float]:
    """ Sum of all transactions in category
    Args:
        category_id (int): 
        user_id (str):UUID Authenticated user
    Returns:
        list[float]: 
    """
    with Session(ENGINE) as session:
        statement = select(Transactions.amount).where(Transactions.sender_id==user_id).where(Transactions.category_id==category_id).where(Transactions.status=='accepted')
        transactions = session.exec(statement).all()
        return transactions