from typing import Literal
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy import exc
from data import database
from sqlmodel import Session, select
from models import user_model
from utils.jwt_token import decode_token
from utils.uuid_generator import generate_uuid
from utils.password_hasher import hash_password
from fastapi.responses import JSONResponse
from models.user_model import UsersDB
from utils.jwt_token import create_token
from pydantic import EmailStr
from email_service.mailjet_config import email_confirmation

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="users/token")


def create_new_user(user: user_model.UserRegister) -> JSONResponse:
    """Creates new user in the DB and sends confirmation email to the user on success. Raises HTTPException on failure.

    Args:
        user (user_model.UserRegister): Valid UserRegister model.

    Raises:
        HTTPException

    Returns:
        JSONResponse
    """
    db_user = UsersDB(id=str(generate_uuid()),
                        username= user.username,
                        password= hash_password(user.password),
                        email= user.email,
                        phone=user.phone_number,
                        role="user")
    try:
        with Session(database.ENGINE) as session:
            session.add(db_user)
            session.commit()
        
        email_confirmation(template_id=4844362,
                           subject="Email confirmation",
                           variables={"mj_confirmation_link": f"http://127.0.0.1:8000/users/{user.username}/confirm"},
                           receiver_email=user.email,
                           reveicer_name=user.username)
        
        return JSONResponse(status_code=201,
                            content={"msg": "New user created."})
    except exc.IntegrityError as e:
         raise HTTPException(status_code=400, detail={"msg": e.args[0].split(":", 1)[-1].strip()})


def get_all_users() -> list[user_model.UserInfo]:

    users: list[user_model.UsersDB] = _get_all_users()

    return [user_model.UserInfo(id=u.id, username=u.username,email=u.email,phone=u.phone, role=u.role,is_blocked=u.is_blocked, is_inactive=u.is_inactive, is_email_confirmed=u.is_email_confirmed) for u in users]


def get_current_user(token: str = Depends(oauth2_scheme)) -> user_model.UserCredentials | HTTPException:
    """Returns currently logged user credentials or raises HTTPException.

    Args:
        token (str): valid JWT token.

    Raises:
        HTTPException

    Returns:
        user_model.UserCredentials | HTTPException
    """
    user = decode_token(token)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid authentication credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
    return user


def get_user_by_username(username: str):
    return _get_user_by_username(username)
    

def get_user_by_id(id: str) -> user_model.UserInfo:
    """Returns UserInfo model of user, searches by ID.

    Args:
        id (str): UUID.

    Returns:
        user_model.UserInfo
    """
    user = _get_user_by_id(id)

    return user_model.UserInfo(id=user.id,
                               username=user.username,
                               email=user.email,
                               phone=user.phone,
                               role=user.role,
                               is_blocked=user.is_blocked,
                               is_inactive=user.is_inactive,
                               is_email_confirmed=user.is_email_confirmed)
    

def update_logged_user(id: str, credentials: user_model.UserInfoUpdate) -> JSONResponse:
    """Accepts user's ID and changes email, phone or password values.  

    Args:
        id (str): UUID.
        credentials (user_model.UserInfoUpdate)

    Raises:
        HTTPException
        
    Returns:
        JSONResponse | HTTPException
    """
    e = database.ENGINE
    
    try:
        with Session(e) as session:
            q = select(user_model.UsersDB).where(UsersDB.id == id)
            result = session.exec(q)
            user = result.first()
            
            if credentials.email != None:
                user.email = credentials.email

            if credentials.phone_number:
                user.phone = credentials.phone_number

            if credentials.new_password:
                user.password =  hash_password(credentials.new_password)

            session.add(user)
            session.commit()

            return JSONResponse(status_code=200, content="User updated.")

    except exc.IntegrityError as e:

        raise HTTPException(status_code=400, detail={"msg": e.args[0].split(":", 1)[-1].strip()})
    

def get_logged_user_password(id: str) -> str | None:
    """Returns user's hashed password by given id or None if not found.

    Args:
        id (str): UUID.

    Returns:
        str | None: returns hashed password or None.
    """
    e = database.ENGINE
    with Session(e) as session:
        q = select(user_model.UsersDB.password).where(UsersDB.id == id)
        result = session.exec(q)
        return result.one_or_none()
    
def access_token(user: UsersDB) -> dict:
    """Creates access token, based on user credentials.

    Args:
        user (UsersDB): Valid user from the DB.

    Returns:
        dict
    """
    token_credentials = user_model.UserCredentials(id=user.id, username=user.username, role=user.role)

    return {"access_token": create_token(token_credentials),
            "token_type": "bearer"}

def block_unblock_user_by_id(id: str, status: Literal["block", "unblock"]) -> JSONResponse:
    """Seaches user by giuven id and changes is_blocked status in the DB. Always returns JSONResponse.

    Args:
        id (str): UUID.
        status: Could be "block" or "unblock".

    Returns:
        JSONResponse
    """
    e = database.ENGINE
    
    try:
        with Session(e) as session:
            q = select(user_model.UsersDB).where(UsersDB.id == id)
            result = session.exec(q)
            user = result.one()
            
            if status == "block":
                user.is_blocked = True
            
            if status == "unblock":
                user.is_blocked = False
            
            session.add(user)
            session.commit()
    except exc.NoResultFound:
        raise HTTPException(status_code=404,
                             detail={"msg":f"User with id {id} not found."})
    
    return JSONResponse({"msg": "Status changed."})


def get_user(seach_by: Literal["username", "email", "phone"],
             val: str) -> user_model.UserInfo | None:
    """Get user by given username, email or phone number. Returns None if user not found.

    Args:
        seach_by: Indicates in which column user is searched. Available params: username, email, phone number.
        val (str): value to seach for the given column.

    Returns:
        user_model.UserInfo | None
    """
    if seach_by == "username":
       user = _get_user_by_username(val)
    
    if seach_by == "email":
       user = _get_user_by_email(val)

    if seach_by == "phone":
        user = _get_user_by_phone(val)

    if user == None:
        return None

    return [user_model.UserInfo(id=user.id,
                               username=user.username,
                               email=user.email,
                               phone=user.phone,
                               role=user.role,
                               is_blocked=user.is_blocked,
                               is_inactive=user.is_inactive,
                               is_email_confirmed=user.is_email_confirmed)]

def deactivate(id: str):
    """Changes is_inacite status to True in DB.Return JSONResponse on success or raise HTTPException.

    Args:
        id (str): valid UUID.

    Raises:
        HTTPException
        
    Returns:
        JSONResponse: Returns status code and message.
    """
    e = database.ENGINE
    try:
        with Session(e) as session:
            q = select(user_model.UsersDB).where(UsersDB.id == id)
            user = session.exec(q).one()

            user.is_inactive = True
            
            session.add(user)
            session.commit()
    except exc.NoResultFound:
        raise HTTPException(status_code=404,
                            detail={"msg":f"User with id {id} not found."})
    
    return JSONResponse({"msg": "Status changed."})


def confirm_email(username: str):
    """Change is_email_confirmed status to True in DB, if username exists, else status code and message is returned.

    Args:
        username (str): valid username.

    Returns:
        JSONResponse: Returns status code and content.
    """
    e = database.ENGINE
    try:
        with Session(e) as session:
            q = select(user_model.UsersDB).where(UsersDB.username == username)
            user = session.exec(q).one()

            user.is_email_confirmed = True
            
            session.add(user)
            session.commit()
    except exc.NoResultFound:
        raise HTTPException(status_code=404,
                            detail={"msg":f"User with username {username} not found."})
    
    return JSONResponse({"msg": "Email confirmed."})


def _get_user_by_username(username: str) -> user_model.UsersDB | None:
    """Seaches user by given username and return UsersDB model if found, else None.

    Args:
        username (str): username

    Returns:
        user_model.UsersDB | None: User(UserDB model) or None
    """
    e = database.ENGINE
    with Session(e) as session:
        q = select(user_model.UsersDB).where(UsersDB.username == username)
        result = session.exec(q)
        return result.one_or_none()
    
def _get_user_by_id(id: str) -> user_model.UsersDB | None:
    """Seaches user by given id and return UsersDB model if found, else None.

    Args:
        id (str): UUID.

    Returns:
        user_model.UsersDB | None: User(UserDB model) or None
    """
    e = database.ENGINE
    with Session(e) as session:
        q = select(user_model.UsersDB).where(UsersDB.id == id)
        result = session.exec(q)
        return result.one_or_none()
    
def _get_user_by_phone(phone: str) -> user_model.UsersDB | None:
    """Seaches user by given phone number and return UsersDB model if found, else None.

    Args:
        phone (str): valid phone number (10 digits.)

    Returns:
        user_model.UsersDB | None: User(UserDB model) or None
    """
    e = database.ENGINE
    with Session(e) as session:
        q = select(user_model.UsersDB).where(UsersDB.phone == phone)
        result = session.exec(q)
        return result.one_or_none()
    
def _get_user_by_email(email: EmailStr) -> user_model.UsersDB | None:
    """Seaches user by given email and return UsersDB model if found, else None.

    Args:
        email (EmailStr): valid email.

    Returns:
        user_model.UsersDB | None: User (UserDB model) or None
    """
    e = database.ENGINE
    with Session(e) as session:
        q = select(user_model.UsersDB).where(UsersDB.email == email)
        result = session.exec(q)
        return result.one_or_none()
    
def _get_all_users() -> list[user_model.UsersDB]:
    """Creates list of all users in db.

    Returns:
        list[user_model.UsersDB]: List of all users (UsersDB model).
    """
    e = database.ENGINE
    with Session(e) as session:
        q = select(user_model.UsersDB)
        result = session.exec(q)
        return result.all()