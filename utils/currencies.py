import requests
from requests.structures import CaseInsensitiveDict


CONVERSION_TAX = 0.0025


def supported_currencies() -> list[str]:
    """
    :return: List of all supported currencies
    """
    url = "https://api.freecurrencyapi.com/v1/currencies"

    headers = CaseInsensitiveDict()
    headers["apikey"] = "39lgV6KXa27AgHfv8qfL9akaIOmgY0gGwBTFwO6S"
    resp = requests.get(url, headers=headers).json()

    return list(resp['data'].keys())


def get_exc_rate(base_curr: str, buy_curr: str) -> float | None:
    """
    Accepts two currencies as parameters and returns the exchange rate between them. If the currencies are the same it
    returns 1.
    :param base_curr: Senders wallet currency.
    :param buy_curr: Receivers wallet currency.
    :return: Exchange rate of the given currencies
    """
    url = "https://api.freecurrencyapi.com/v1/latest"
    headers = CaseInsensitiveDict()

    headers["apikey"] = "39lgV6KXa27AgHfv8qfL9akaIOmgY0gGwBTFwO6S"
    params = {
        "currencies": buy_curr.upper(),
        "base_currency": base_curr.upper()
    }

    response = requests.get(url, headers=headers, params=params)

    if response.status_code == 200:
        json_response = response.json()
        exchange_rate = json_response["data"][buy_curr.upper()]
        return exchange_rate
    else:
        return None


def calculate_conversion_amount(base_amount: float, exc_rat: float) -> float:
    """
    Calculates the amount in the 'buy' currency
    :param base_amount: The amount in base currency.
    :param exc_rat: Given exchange rate.
    :return: The amount in the 'buy' currency.
    """
    return base_amount * exc_rat
