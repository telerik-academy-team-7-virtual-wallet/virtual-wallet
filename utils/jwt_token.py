from fastapi import HTTPException
from settings import Jwt_credentials
from models.user_model import UserCredentials
import jwt


def create_token(user: UserCredentials):
    payload = user.dict()
    # payload["exp"] = datetime.now(tz=timezone.utc) + timedelta(hours=6)
    token = jwt.encode(payload, key=Jwt_credentials.SECRET_KEY, algorithm=Jwt_credentials.ALGORITHM)
    return token


def decode_token(token: str):
    try:
        decoded_token = jwt.decode(token, key=Jwt_credentials.SECRET_KEY, algorithms=Jwt_credentials.ALGORITHM)
    except jwt.exceptions.ExpiredSignatureError as e:
        return HTTPException(status_code=403,
                            detail={"msg": e.args[0]})

    return UserCredentials(id=decoded_token["id"],
                            username=decoded_token["username"],
                            role=decoded_token["role"])




