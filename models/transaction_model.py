from sqlmodel import SQLModel, Field
from typing import Optional
from uuid import UUID, uuid4
from pydantic import BaseModel
from datetime import datetime


class Transactions(SQLModel, table=True):
    __tablename__ = 'transactions'
    id: Optional[str] = Field(default_factory=lambda: str(uuid4()), primary_key=True)
    sender_id: str
    receiver_id: str
    amount: float = Field(gt=0)
    time: datetime | None
    status: str = 'not confirmed'
    category_id: int | None = None
    sender_wallet_id: str
    receiver_wallets_id: str | None = None
    currency: str
    exchange_rate: float | None = None


class CreateTransaction(BaseModel):
    id: str | None = None
    receiver_id: str | None
    amount: float = Field(gt=0)
    category: Optional[str]
    sender_wallet_id: str
    receiver_wallet_id: str
    exchange_rate: float | None = None


class UpdateTransaction(BaseModel):
    receiver_id: str | None
    amount: float | None
    sender_wallet_id: str | None
    category: Optional[str]


class ViewTransaction(BaseModel):
    id: UUID | None = None
    direction: str | None = None
    recipient_id: str | None = None
    sender_id: str | None = None
    amount: float
    time: datetime
