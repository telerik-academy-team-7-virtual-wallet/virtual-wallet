from typing import Literal, Optional
from uuid import UUID
from sqlmodel import SQLModel, Field as sql_field
from pydantic import BaseModel, Field, EmailStr, validator

class UsersDB(SQLModel, table=True):

    __tablename__ = "users"

    id: str = sql_field(primary_key=True)
    username: str
    password: str
    email: str
    phone: str
    role: str
    is_blocked: int = 0
    is_inactive: int = 0
    is_email_confirmed: int = 0

class UserRegister(BaseModel):
    username: str = Field(..., min_length=2, max_length=20)
    password: str = Field(..., min_length=8, max_length=20, regex="^(?=.*\d)(?=.*[a-zA-Z])[0-9a-zA-Z$&+,:;=?@#|'<>.-^*()%!]{8,20}$")
    repeat_password: str = Field(..., min_length=8, max_length=20, regex="^(?=.*\d)(?=.*[a-zA-Z])[0-9a-zA-Z$&+,:;=?@#|'<>.-^*()%!]{8,20}$")
    phone_number: str = Field(..., min_length=10, max_length=10)
    email: EmailStr

    @validator('repeat_password')
    def passwords_match(cls, v, values, **kwargs):
        if 'password' in values and v != values['password']:
            raise ValueError('passwords do not match')
        return v
    
class UserDisplay(BaseModel):
    username: str
    phone_number: str
    EmailStr: str


class UserLogin(BaseModel):
    username: str
    password: str

class UserCredentials(BaseModel):
    id: str
    username: str
    role: str


class UserInfo(BaseModel):
    id: str
    username: str
    email: str
    phone: str
    role: str
    is_blocked: int
    is_inactive: int
    is_email_confirmed: int


class UserInfoUpdate(BaseModel):
    phone_number: str  | None = Field(None, min_length=10, max_length=10)
    email: EmailStr | None = None
    old_password: str | None = None
    new_password: str | None = Field(None, min_length=8, max_length=20, regex="^(?=.*\d)(?=.*[a-zA-Z])[0-9a-zA-Z$&+,:;=?@#|'<>.-^*()%!]{8,20}$")

    @validator('new_password')
    def passwords_match(cls, v, values, **kwargs):
        if values['old_password'] == None:
            raise ValueError('Old password or new password is missing.')
        return v