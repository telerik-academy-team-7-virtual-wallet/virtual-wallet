from sqlmodel import SQLModel, Field
from typing import Optional
from pydantic import BaseModel
from datetime import datetime


class Transfer(SQLModel, table=True):
    __tablename__ = 'transfers'
    id: int = Field(primary_key=True)
    user_id: str = Field(min_length=36, max_length=36)
    bank_cards_id: str = Field(min_length=36, max_length=36)
    wallets_id: str = Field(min_length=36, max_length=36)
    amount: float
    time: datetime


class TransferMoney(BaseModel):
    amount: float 
    bank_card: str = Field(min_length=36, max_length=36)

    


class Withdraw(BaseModel):
    amount: float
    bank_card: str = Field(min_length=36, max_length=36)
