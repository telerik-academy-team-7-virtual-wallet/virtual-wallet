from sqlmodel import SQLModel, Field



class WalletsHasUsers(SQLModel, table=True):
    __tablename__ = "wallets_has_users"
    wallet_id: str = Field(default=None, foreign_key="wallets.id", primary_key=True)
    user_id: str = Field(foreign_key="users.id", primary_key=True)


