from sqlmodel import SQLModel, Field
from typing import Literal
from pydantic import BaseModel
from utils.currencies import supported_currencies 

class Wallets(SQLModel, table=True):
    __tablename__ = 'wallets'
    id: str = Field(primary_key=True)
    wallet_name: str = Field(min_length=3, max_length=45)
    amount: float = Field(default=0)
    currency: str = Field(max_length=3)
    admin_id: str



currency_lst = supported_currencies()
lst = tuple(cur for cur in currency_lst)

class CreateWallet(BaseModel):
    wallet_name: str = Field(min_length=3, max_length=45)
    currency: Literal[lst] = 'BGN'



# cards = get_current_user_bank_cards()







    # @validator('currency')
    # def is_curr_valid(cls, v):
    #     v = v.upper()
    #     if v not in currencies.supported_currencies():
    #         raise ValueError('currency not valid')

    #     return v



