from sqlmodel import SQLModel, Field
from typing import Optional
from pydantic import BaseModel

class Contacts(SQLModel, table=True):
    __tablename__ = 'contacts'
    id: Optional[int] = Field(default=None, primary_key=True)
    users_id: str = Field(min_length=36, max_length=36, foreign_key='users.id')
    users_id1: str = Field(min_length=36, max_length=36, foreign_key='users.id')



class AddUserInContact_list(BaseModel):
    username: str