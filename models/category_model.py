from sqlmodel import Field, SQLModel
from pydantic import BaseModel
from typing import Optional

class Categories(SQLModel, table=True):
    __tablename__ = 'categories'
    id: Optional[int] = Field(default=None, primary_key=True)
    category: str = Field(min_length=3, max_length=45)



class CreateCategory(BaseModel):
    category: str = Field(min_length=3, max_length=45)


class UpdateCategory(BaseModel):
    category: str = Field(min_length=3, max_length=45)