from fastapi_pagination import add_pagination
import uvicorn
from fastapi import FastAPI
from routers.transactions import transactions_router
from routers.users import users_router
from routers.wallets import wallets_router
from routers.categories import categories_router
from routers.transfers import transfers_router
from routers.contact_list import contacts_router

app = FastAPI(debug=True)

add_pagination(app)

app.include_router(users_router)
app.include_router(wallets_router)
app.include_router(categories_router)
app.include_router(transactions_router)
app.include_router(transfers_router)
app.include_router(contacts_router)

if __name__ == '__main__':
    uvicorn.run('main:app', host='127.0.0.1', port=8000, reload=True)
