-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema virtual_wallet
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema virtual_wallet
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `virtual_wallet` DEFAULT CHARACTER SET latin1 ;
USE `virtual_wallet` ;

-- -----------------------------------------------------
-- Table `virtual_wallet`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `virtual_wallet`.`users` (
  `id` VARCHAR(100) NOT NULL,
  `username` VARCHAR(20) NOT NULL,
  `password` VARCHAR(120) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `phone` VARCHAR(11) NOT NULL,
  `role` VARCHAR(15) NOT NULL,
  `is_blocked` TINYINT(4) NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
  UNIQUE INDEX `phone_UNIQUE` (`phone` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `virtual_wallet`.`bank_cards`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `virtual_wallet`.`bank_cards` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `card_number` VARCHAR(16) NOT NULL,
  `card_holder` VARCHAR(30) NOT NULL,
  `ccv` VARCHAR(3) NOT NULL,
  `expiration` DATE NOT NULL,
  `users_id` VARCHAR(100) NOT NULL,
  `currency` VARCHAR(3) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `card_number_UNIQUE` (`card_number` ASC) VISIBLE,
  INDEX `fk_bank_cards_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_bank_cards_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `virtual_wallet`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `virtual_wallet`.`categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `virtual_wallet`.`categories` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `category` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `category_UNIQUE` (`category` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `virtual_wallet`.`contacts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `virtual_wallet`.`contacts` (
  `users_id` VARCHAR(100) NOT NULL,
  `users_id1` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`users_id`, `users_id1`),
  INDEX `fk_users_has_users_users2_idx` (`users_id1` ASC) VISIBLE,
  INDEX `fk_users_has_users_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_users_has_users_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `virtual_wallet`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_users_users2`
    FOREIGN KEY (`users_id1`)
    REFERENCES `virtual_wallet`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `virtual_wallet`.`wallets`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `virtual_wallet`.`wallets` (
  `id` VARCHAR(100) NOT NULL,
  `amount` DECIMAL(10,0) NULL DEFAULT NULL,
  `currency` VARCHAR(3) NULL DEFAULT NULL,
  `admin_id` VARCHAR(100) NOT NULL,
  `wallet_id` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_wallets_users1_idx` (`admin_id` ASC) VISIBLE,
  INDEX `fk_wallets_wallets1_idx` (`wallet_id` ASC) VISIBLE,
  CONSTRAINT `fk_wallets_users1`
    FOREIGN KEY (`admin_id`)
    REFERENCES `virtual_wallet`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_wallets_wallets1`
    FOREIGN KEY (`wallet_id`)
    REFERENCES `virtual_wallet`.`wallets` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `virtual_wallet`.`transactions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `virtual_wallet`.`transactions` (
  `id` VARCHAR(100) NOT NULL,
  `sender` VARCHAR(100) NOT NULL,
  `receiver` VARCHAR(100) NOT NULL,
  `amount` DECIMAL(10,0) NOT NULL,
  `time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  `status` VARCHAR(10) NULL DEFAULT NULL,
  `category_id` INT(11) NULL DEFAULT NULL,
  `sender_wallet_id` VARCHAR(100) NOT NULL,
  `receiver_wallets_id` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_transactions_users_idx` (`sender` ASC) VISIBLE,
  INDEX `fk_transactions_users1_idx` (`receiver` ASC) VISIBLE,
  INDEX `fk_transactions_categories1_idx` (`category_id` ASC) VISIBLE,
  INDEX `fk_transactions_wallets1_idx` (`sender_wallet_id` ASC) VISIBLE,
  INDEX `fk_transactions_wallets2_idx` (`receiver_wallets_id` ASC) VISIBLE,
  CONSTRAINT `fk_transactions_categories1`
    FOREIGN KEY (`category_id`)
    REFERENCES `virtual_wallet`.`categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_transactions_users`
    FOREIGN KEY (`sender`)
    REFERENCES `virtual_wallet`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_transactions_users1`
    FOREIGN KEY (`receiver`)
    REFERENCES `virtual_wallet`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_transactions_wallets1`
    FOREIGN KEY (`sender_wallet_id`)
    REFERENCES `virtual_wallet`.`wallets` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_transactions_wallets2`
    FOREIGN KEY (`receiver_wallets_id`)
    REFERENCES `virtual_wallet`.`wallets` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `virtual_wallet`.`transfers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `virtual_wallet`.`transfers` (
  `wallet_id` VARCHAR(100) NOT NULL,
  `bank_card_id` INT(11) NOT NULL,
  `amount` DECIMAL(10,0) NOT NULL,
  `time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (`wallet_id`, `bank_card_id`),
  INDEX `fk_wallets_has_bank_cards_bank_cards1_idx` (`bank_card_id` ASC) VISIBLE,
  INDEX `fk_wallets_has_bank_cards_wallets1_idx` (`wallet_id` ASC) VISIBLE,
  CONSTRAINT `fk_wallets_has_bank_cards_bank_cards1`
    FOREIGN KEY (`bank_card_id`)
    REFERENCES `virtual_wallet`.`bank_cards` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_wallets_has_bank_cards_wallets1`
    FOREIGN KEY (`wallet_id`)
    REFERENCES `virtual_wallet`.`wallets` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `virtual_wallet`.`wallets_has_users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `virtual_wallet`.`wallets_has_users` (
  `wallet_id` VARCHAR(100) NOT NULL,
  `user_id` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`wallet_id`, `user_id`),
  INDEX `fk_wallets_has_users_users1_idx` (`user_id` ASC) VISIBLE,
  INDEX `fk_wallets_has_users_wallets1_idx` (`wallet_id` ASC) VISIBLE,
  CONSTRAINT `fk_wallets_has_users_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `virtual_wallet`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_wallets_has_users_wallets1`
    FOREIGN KEY (`wallet_id`)
    REFERENCES `virtual_wallet`.`wallets` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
