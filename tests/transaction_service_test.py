import unittest
from unittest.mock import Mock, patch
from models import user_model, wallet_model
from models.transaction_model import Transactions, CreateTransaction
from services import transaction_service
from tests import wallet_service_test
from fastapi.responses import JSONResponse
from sqlmodel import create_engine, Session

FAKE_USER = user_model.UserCredentials(id='4a9d05bc-78dc-4804-b81a-d2d1efb1effc', username='fake_user', role='user')
FAKE_RECEIVER = [user_model.UserInfo(id='5b8d05bc-78dc-4804-b81a-d2d1efb1easd', username='fake_receiver',
                                     email='fake_receiver@email.com', phone='0123456789',
                                     role='user', is_blocked=0, is_inactive=0, is_email_confirmed=0)]

FAKE_SENDER_WALLET = wallet_model.Wallets(id="14df1b91-ad69-4185-bab1-6b50c3da3082", wallet_name="fake_sender_wallet",
                                          amount=1000, currency="BGN", admin_id="4a9d05bc-78dc-4804-b81a-d2d1efb1effc")

FAKE_RECEIVER_WALLET = wallet_model.Wallets(id="15df1b91-ad69-4185-bab1-6b50c3da3082",
                                            wallet_name="fake_receiver_wallet",
                                            amount=1000, currency="BGN",
                                            admin_id="5b8d05bc-78dc-4804-b81a-d2d1efb1easd")

FAKE_WALLETS_IDS = ["14df1b91-ad69-4185-bab1-6b50c3da3082", "15df1b91-ad69-4185-bab1-6b50c3da3082"]

FAKE_CREATE_TXN = CreateTransaction(receiver_id='5b8d05bc-78dc-4804-b81a-d2d1efb1easd',
                                    amount=500, sender_wallet_id='14df1b91-ad69-4185-bab1-6b50c3da3082',
                                    receiver_wallet_id='15df1b91-ad69-4185-bab1-6b50c3da3082')

FAKE_TRANSACTION_DB = Transactions(id='0000b14f7d52480daf82f6ff15ed4000',
                                   sender_id='4a9d05bc-78dc-4804-b81a-d2d1efb1effc',
                                   receiver_id='5b8d05bc-78dc-4804-b81a-d2d1efb1easd',
                                   amount=500, sender_wallet_id='14df1b91-ad69-4185-bab1-6b50c3da3082',
                                   receiver_wallet_id='15df1b91-ad69-4185-bab1-6b50c3da3082', currency='BGN')

FAKE_TXN_LST = [Transactions(id='1000b14f7d52480daf82f6ff15ed4000', sender_id='4a9d05bc-78dc-4804-b81a-d2d1efb1effc',
                             receiver_id='5b8d05bc-78dc-4804-b81a-d2d1efb1easd',
                             amount=500, status='pending', sender_wallet_id='14df1b91-ad69-4185-bab1-6b50c3da3082',
                             receiver_wallet_id='15df1b91-ad69-4185-bab1-6b50c3da3082', currency='BGN'),
                Transactions(id='2000b14f7d52480daf82f6ff15ed4000', sender_id='5b8d05bc-78dc-4804-b81a-d2d1efb1easd',
                             receiver_id='5b8d05bc-78dc-4804-b81a-d2d1efb1easd',
                             amount=500, status='pending', sender_wallet_id='14df1b91-ad69-4185-bab1-6b50c3da3082',
                             receiver_wallet_id='15df1b91-ad69-4185-bab1-6b50c3da3082', currency='BGN')]
# id: Optional[str] = Field(default_factory=lambda: str(uuid4()), primary_key=True)
#     sender_id: str
#     receiver_id: str
#     amount: float = Field(gt=0)
#     time: datetime | None
#     status: str = 'not confirmed'
#     category_id: int | None = None
#     sender_wallet_id: str
#     receiver_wallets_id: str | None = None
#     currency: str
#     exchange_rate: float | None = None
fake_engine = create_engine(f"sqlite:///fake_file_name", echo=True)


class FakeResult():

    def one_or_none():
        pass

    def all():
        pass

    def first():
        pass

    def one():
        pass


class TransactionService_Should(unittest.TestCase):
    @patch("services.wallet_service.get_by_id", Mock(return_value=FAKE_SENDER_WALLET))
    @patch("services.wallet_service.get_all_wallet_ids_for_auth_user", Mock(return_value=FAKE_WALLETS_IDS))
    @patch("services.user_service.get_user", Mock(return_value=FAKE_RECEIVER))
    @patch("services.wallet_service.check_if_user_owns_wallet", Mock(return_value=FAKE_RECEIVER_WALLET))
    @patch("services.transaction_service.get_exc_rate", Mock(return_value=1.0))
    @patch.object(Session, "refresh", Mock(return_value=FAKE_CREATE_TXN))
    @patch.object(Session, "add", Mock(return_value="ok"))
    @patch.object(Session, "commit", Mock(return_value="ok"))
    @patch("services.transaction_service.ENGINE", Mock(return_value=fake_engine))
    def test_prepare_transaction_returnCreateTransaction(self):
        result: CreateTransaction = transaction_service.prepare_transaction(FAKE_CREATE_TXN, FAKE_USER, 'fake', 'fake')
        self.assertIsInstance(result, CreateTransaction)

    @patch("services.transaction_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "first", Mock(return_value=FAKE_TRANSACTION_DB))
    def test_get_by_id_return_Transaction_on_success(self):
        # act
        result: Transactions = transaction_service.get_by_id(txn_id='0000b14f7d52480daf82f6ff15ed4000')

        # assert
        self.assertIsInstance(result, Transactions)

    @patch("services.transaction_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "all", Mock(return_value=FAKE_TXN_LST))
    def test_get_pending_transactions_auth_user_returnListOfTransactions(self):
        result: list[Transactions] = transaction_service.get_pending_transactions_auth_user(FAKE_USER)
        self.assertEqual(len(result), 2)
