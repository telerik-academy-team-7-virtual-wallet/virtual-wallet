import unittest
from unittest.mock import Mock, patch
from fastapi import HTTPException
from services import user_service
from models import user_model
from fastapi.responses import JSONResponse
from sqlmodel import create_engine, Session
from sqlalchemy import exc


class FakeResult():
    def one_or_none():
        pass

    def all():
        pass

FAKE_USER_REGISTER = user_model.UserRegister(username="fake_user",
                                    password="Aa!12345678",
                                    repeat_password="Aa!12345678",
                                    phone_number="0890909090",
                                    email="fake_user@email.com")

LIST_FAKE_DB_USERS = [
    user_model.UsersDB(id="1", username="fake_user_1",password="Aa!@#$1234",email="user_1@email.com",phone="1234567890", role="user", is_blocked=0,is_inactive=0,is_email_confirmed=0),
    user_model.UsersDB(id="2", username="fake_user_2",password="Aa!@#$1234",email="user_2@email.com",phone="1234567891", role="user", is_blocked=0,is_inactive=0,is_email_confirmed=0)
]

FAKE_USER_INFO_UPDATE = user_model.UserInfoUpdate(phone_number="1112223334")

fake_engine = create_engine(f"sqlite:///fake_file_name", echo=True)


def raise_integrityError():
    raise(exc.IntegrityError(statement="SELECT*",params="fake_params",orig="fake_orig"))

def raise_noResultFound():
    raise(exc.NoResultFound(["fake_error"]))

class CategoryUser_serviceShould(unittest.TestCase):
    
    @patch("services.user_service.database")
    @patch("services.user_service.email_confirmation")
    def test_create_new_user_returnJSONResponse_on_success(self, f_engine, email):
        #arrange
        f_engine.return_value = fake_engine
        email.return_value = "ok"
        #act
        result: JSONResponse = user_service.create_new_user(FAKE_USER_REGISTER)

        #assert
        self.assertEqual(result.status_code, 201)

    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch("services.user_service.email_confirmation", Mock(return_value="ok"))
    @patch.object(Session, "commit", Mock(side_effect=raise_integrityError))
    def test_create_new_user_returnHTTPException_on_raisedIntegrityError(self):
        with self.assertRaises(HTTPException):
            user_service.create_new_user(FAKE_USER_REGISTER)      

    @patch("services.user_service._get_all_users")
    def test_get_all_users_returnListOfUsers_on_success(self, all_users):
        #arrange
        all_users.return_value = LIST_FAKE_DB_USERS
        #act
        result: list[user_model.UserInfo] = user_service.get_all_users()

        #assert
        self.assertEqual(len(result), 2)

    @patch("services.user_service._get_all_users")
    def test_get_all_users_returnEmptyList_when_noUsers(self, all_users):
        #arrange
        all_users.return_value = []
        #act
        result: list[user_model.UserInfo] = user_service.get_all_users()

        #assert
        self.assertEqual(result, [])
    
    @patch("services.user_service.decode_token")
    def test_get_current_user_returnUser_on_success(self, fake_decode):
        #arrange
        fake_decode.return_value = user_model.UserCredentials(id=1,username="fake_user",role="user")
        fake_token = "fake_token"
        #act
        result: user_model.UserCredentials = user_service.get_current_user(fake_token)

        #assert
        self.assertIsInstance(result, user_model.UserCredentials)

    @patch("services.user_service.decode_token")
    def test_get_current_user_raiseHTTPExcepton_on_failure(self, fake_decode):
        #arrange
        fake_decode.return_value = None
        fake_token = "fake_token"
        
        #act & assert
        with self.assertRaises(HTTPException) as e:
            user_service.get_current_user(fake_token)

    @patch("services.user_service._get_user_by_username")
    def test_get_user_by_username_returnUser_on_success(self, fake_get_user_by_username):
        #arrange
        fake_get_user_by_username.return_value = LIST_FAKE_DB_USERS[0]
        #act
        result: user_model.UsersDB = user_service.get_user_by_username("fake_user_1")

        #assert
        self.assertIsInstance(result, user_model.UsersDB)

    @patch("services.user_service._get_user_by_username")
    def test_get_user_by_username_returnNone_on_unexistingUsername(self, fake_get_user_by_username):
        #arrange
        fake_get_user_by_username.return_value = None
        #act
        result: user_model.UsersDB = user_service.get_user_by_username("fake_user_1")

        #assert
        self.assertEqual(result, None)
    
    @patch("services.user_service.database")
    @patch.object(Session,"add")
    def test_update_logged_user_returnJSONResponse_on_success(self, f_engine, s_add):
        #arrange
        f_engine.return_value = fake_engine
        s_add.return_value = None
        #act
        result: JSONResponse = user_service.update_logged_user(id=1, credentials=FAKE_USER_INFO_UPDATE)

        #assert
        self.assertEqual(result.status_code, 200)

    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch.object(Session,"add", Mock(return_value="ok"))
    @patch.object(Session,"commit", Mock(side_effect=raise_integrityError))
    def test_update_logged_user_raiseHTTPException_on_raisedIntegrityError(self):
        with self.assertRaises(HTTPException):
            user_service.update_logged_user(id=1, credentials=FAKE_USER_INFO_UPDATE)

    @patch("services.user_service.create_token")
    def test_access_token_returnDict_on_success(self, fake_token):
        #arrange
        fake_token.return_value = "fake_token"

        #act
        result: dict = user_service.access_token(LIST_FAKE_DB_USERS[0])

        #assert
        self.assertIsInstance(result,dict)

    @patch("services.user_service.create_token")
    def test_access_token_return_token_type_and_access_token_on_success(self, fake_token):
        #arrange
        fake_token.return_value = "fake_token"

        #act
        result: dict = user_service.access_token(LIST_FAKE_DB_USERS[0])

        #assert
        self.assertEqual(result["token_type"], "bearer")
        self.assertEqual(result["access_token"], "fake_token")

    @patch("services.user_service.database")
    @patch.object(Session, "add")
    @patch.object(Session, "commit")
    def test_block_unblock_user_by_id_return_JSONResponse_on_statusEqualBlcok(self, f_engine, s_add, s_commit):
        #arrange
        f_engine.return_value = fake_engine
        s_add.return_value = "ok"
        s_commit.return_value = "ok"
        #act
        result: JSONResponse = user_service.block_unblock_user_by_id(id=1, status="block")

        #assert
        self.assertEqual(result.status_code, 200)

    @patch("services.user_service.database")
    @patch.object(Session, "add")
    @patch.object(Session, "commit")
    def test_block_unblock_user_by_id_return_JSONResponse_on_statusEqualUnlcok(self, f_engine, s_add, s_commit):
        #arrange
        f_engine.return_value = fake_engine
        s_add.return_value = "ok"
        s_commit.return_value = "ok"
        #act
        result: JSONResponse = user_service.block_unblock_user_by_id(id=1, status="unblock")

        #assert
        self.assertEqual(result.status_code, 200)

    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch.object(Session, "add", Mock(return_value="ok"))
    @patch.object(Session, "commit", Mock(side_effect=raise_noResultFound))
    def test_block_unblock_user_by_id_raise_HTTPException_on_catchedNoResultFoundError(self):
        with self.assertRaises(HTTPException):
            user_service.block_unblock_user_by_id(id=1, status="unblock")


    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch("services.user_service._get_user_by_username", Mock(return_value=LIST_FAKE_DB_USERS[0]))
    def test_get_user_return_UserInfoObject_with_seach_byUsername(self):
        #act
        result: user_model.UserInfo = user_service.get_user(seach_by="username", val="fake_name")
        
        #assert
        self.assertIsInstance(result[0], user_model.UserInfo)
        self.assertEqual(result[0].username, "fake_user_1")

    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch("services.user_service._get_user_by_email", Mock(return_value=LIST_FAKE_DB_USERS[0]))
    def test_get_user_return_UserInfoObject_with_seach_byEmail(self):
        #act
        result: user_model.UserInfo = user_service.get_user(seach_by="email", val="fake@email.com")
        
        #assert
        self.assertIsInstance(result[0], user_model.UserInfo)
        self.assertEqual(result[0].username, "fake_user_1")

    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch("services.user_service._get_user_by_phone", Mock(return_value=LIST_FAKE_DB_USERS[0]))
    def test_get_user_return_UserInfoObject_with_seach_byPhone(self):
        #act
        result: user_model.UserInfo = user_service.get_user(seach_by="phone", val="0890909090")
        
        #assert
        self.assertIsInstance(result[0], user_model.UserInfo)
        self.assertEqual(result[0].username, "fake_user_1")

    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch("services.user_service._get_user_by_phone", Mock(return_value=None))
    def test_get_user_return_None_if_userNotFound(self):
        #act
        result: user_model.UserInfo = user_service.get_user(seach_by="phone", val="0890909090")
        
        #assert
        self.assertEqual(result, None)

    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch.object(Session, "add", Mock(return_value="ok"))
    @patch.object(Session, "commit", Mock(return_value="ok"))
    def test_deactivate_return_JSONResponse_on_success(self):
        #act
        result: JSONResponse = user_service.deactivate(id=1)
        
        #assert
        self.assertIsInstance(result, JSONResponse)
        self.assertEqual(result.status_code, 200)

    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch.object(Session, "add", Mock(return_value="ok"))
    @patch.object(Session, "commit", Mock(side_effect=raise_noResultFound))
    def test_deactivate_raise_HTTPException_on_cathedNoResultFoundError(self):
        with self.assertRaises(HTTPException):
            user_service.deactivate(id=1)

    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch.object(Session, "add", Mock(return_value="ok"))
    @patch.object(Session, "commit", Mock(return_value="ok"))
    def test_confirm_email_return_JSONResponse_on_success(self):
        #act
        result: JSONResponse = user_service.confirm_email(username="fake_username")
        
        #assert
        self.assertIsInstance(result, JSONResponse)
        self.assertEqual(result.status_code, 200)   

    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch.object(Session, "add", Mock(return_value="ok"))
    @patch.object(Session, "commit", Mock(side_effect=raise_noResultFound))
    def test_confirm_email_raise_HTTPException_on_catchedNoResultFoundError(self):
        with self.assertRaises(HTTPException):
            user_service.confirm_email(username="fake_username")

    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "one_or_none", Mock(return_value="fake_password"))
    def test_get_logged_user_password_return_str_on_success(self):
        #act
        result: str = user_service.get_logged_user_password(id=1)
        
        #assert
        self.assertEqual(result, "fake_password") 

    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "one_or_none", Mock(return_value=None))
    def test_get_logged_user_password_return_None_if_idNotFound(self):
        #act
        result: str = user_service.get_logged_user_password(id=111)
        
        #assert
        self.assertIsNone(result) 

    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "one_or_none", Mock(return_value=LIST_FAKE_DB_USERS[0]))
    def test__get_user_by_username_return_user_on_success(self):
        #act
        result: user_model.UsersDB = user_service._get_user_by_username(username="fake_user")
        
        #assert
        self.assertIsInstance(result, user_model.UsersDB)
    
    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "one_or_none", Mock(return_value=None))
    def test__get_user_by_username_return_None_if_userNotFound(self):
        #act
        result: None = user_service._get_user_by_username(username="fake_user")
        
        #assert
        self.assertIsNone(result) 

    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "one_or_none", Mock(return_value=LIST_FAKE_DB_USERS[0]))
    def test__get_user_by_id_return_user_on_success(self):
        #act
        result: user_model.UsersDB = user_service._get_user_by_id(id=1)
        
        #assert
        self.assertIsInstance(result, user_model.UsersDB)

    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "one_or_none", Mock(return_value=None))
    def test__get_user_by_id_return_None_if_userNotFound(self):
        #act
        result: None = user_service._get_user_by_id(id=1)
        
        #assert
        self.assertIsNone(result)

    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "one_or_none", Mock(return_value=LIST_FAKE_DB_USERS[0]))
    def test__get_user_by_phone_return_user_on_success(self):
        #act
        result: user_model.UsersDB = user_service._get_user_by_phone(phone="1231231231")
        
        #assert
        self.assertIsInstance(result, user_model.UsersDB) 

    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "one_or_none", Mock(return_value=None))
    def test__get_user_by_phone_return_None_if_userNotFound(self):
        #act
        result: None = user_service._get_user_by_phone(phone="1231231231")
        
        #assert
        self.assertIsNone(result)

    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "one_or_none", Mock(return_value=LIST_FAKE_DB_USERS[0]))
    def test__get_user_by_email_return_user_on_success(self):
        #act
        result: user_model.UsersDB = user_service._get_user_by_email(email="test@test.com")
        
        #assert
        self.assertIsInstance(result, user_model.UsersDB) 

    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "one_or_none", Mock(return_value=None))
    def test__get_user_by_email_return_None_if_userNotFound(self):
        #act
        result: None = user_service._get_user_by_email(email="test@test.com")
        
        #assert
        self.assertIsNone(result)
    
    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "all", Mock(return_value=LIST_FAKE_DB_USERS))
    def test__get_all_users_return_listOfUsers_on_success(self):
        #act
        result: list[user_model.UsersDB] = user_service._get_all_users()
        
        #assert
        self.assertEqual(len(result), 2) 

    @patch("services.user_service.database", Mock(return_value=fake_engine))
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "all", Mock(return_value=[]))
    def test__get_all_users_return_emptyList_if_noUsersInDB(self):
        #act
        result: list[user_model.UsersDB] = user_service._get_all_users()
        
        #assert
        self.assertEqual(len(result), 0) 