import unittest
from unittest.mock import Mock, patch
from models import user_model
from models.bank_card_model import BankCardsDB, BankCardAdd
from services import bank_card_service
from fastapi.responses import JSONResponse
from sqlmodel import create_engine, Session


FAKE_USER = user_model.UserCredentials(id='1', username='testuser', role='user')

FAKE_CARD = BankCardsDB(id='696ebdf0-220e-4765-86f9-4cc71265abcd', card_number='0000011027547343',
                        card_holder='Test User', ccv='999', expiration='2024-05-30', user_id='1', currency='BGN')

FAKE_CARD_2 = BankCardsDB(id='000ebdf0-220e-4765-86f9-4cc71265abcd', card_number='1000011027547343',
                          card_holder='Test User', ccv='999', expiration='2024-05-30', user_id='1', currency='BGN')

FAKE_CARD_ADD = BankCardAdd(card_number='1234567890123456', card_holder='Test User', ccv='999', expiration='2024-05-30',
                            currency='BGN')

LIST_FAKE_CARDS = [BankCardsDB(id='000ebdf0-220e-4765-86f9-4cc71265abcd', card_number='1000011027547343',
                               card_holder='Test User', ccv='999', expiration='2024-05-30', user_id='1',
                               currency='BGN'),
                   BankCardsDB(id='222ebdf0-220e-4765-86f9-4cc71265abcd', card_number='2000011027547343',
                               card_holder='Test User 2', ccv='999', expiration='2025-05-30', user_id='1',
                               currency='BGN')
                   ]

fake_engine = create_engine(f"sqlite:///fake_file_name", echo=True)


class FakeResult():

    def one_or_none():
        pass

    def all():
        pass

    def first():
        pass

    def one():
        pass


class BankCardService_Should(unittest.TestCase):

    @patch.object(Session, "refresh", Mock(return_value=FAKE_CARD))
    @patch.object(Session, "add", Mock(return_value="ok"))
    @patch.object(Session, "commit", Mock(return_value="ok"))
    @patch("services.bank_card_service.ENGINE", Mock(return_value=fake_engine))
    def test_create_card(self):
        result: BankCardsDB = bank_card_service.create(FAKE_CARD_ADD, FAKE_USER)
        self.assertIsInstance(result, BankCardsDB)

    @patch("services.bank_card_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "first", Mock(return_value=FAKE_CARD))
    def test_get_card_by_id_return_bank_card_on_success(self):
        # act
        result: BankCardsDB = bank_card_service.get_card_by_id(id='696ebdf0-220e-4765-86f9-4cc71265abcd')

        # assert
        self.assertIsInstance(result, BankCardsDB)

    @patch("services.bank_card_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "first", Mock(return_value=None))
    def test_get_card_by_id_return_None_if_idNotFound(self):
        # act
        result: BankCardsDB = bank_card_service.get_card_by_id(id='0000')

        # assert
        self.assertIsNone(result)

    @patch("services.bank_card_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "all", Mock(return_value=LIST_FAKE_CARDS))
    def test_get_cards_by_user_return_listOfWallets_on_success(self):
        # act
        result: list[BankCardsDB] = bank_card_service.get_cards_by_user(FAKE_USER)

        # assert
        self.assertEqual(len(result), 2)

    @patch("services.bank_card_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "all", Mock(return_value=[]))
    def test_get_cards_by_user_return_listOfWallets_on_success(self):
        # act
        result: list[BankCardsDB] = bank_card_service.get_cards_by_user(FAKE_USER)

        # assert
        self.assertEqual(len(result), 0)

    @patch("services.bank_card_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "first", Mock(return_value=FAKE_CARD))
    def test_exists_return_bank_card_on_success(self):
        # act
        result: BankCardsDB = bank_card_service.get_card_by_id(id='696ebdf0-220e-4765-86f9-4cc71265abcd')

        # assert
        self.assertIsInstance(result, BankCardsDB)

    @patch("services.bank_card_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "first", Mock(return_value=None))
    def test_exists_return_None_if_idNotFound(self):
        # act
        result: BankCardsDB = bank_card_service.get_card_by_id(id='0000')

        # assert
        self.assertIsNone(result)

    @patch("services.bank_card_service.get_cards_by_user", Mock(return_value=LIST_FAKE_CARDS))
    @patch("services.bank_card_service.exists", Mock(return_value=FAKE_CARD_2))
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "one", Mock(return_value=BankCardsDB))
    @patch.object(Session, "delete", Mock(return_value=1))
    @patch.object(Session, "commit", Mock(return_value="ok"))
    def test_delete_return_JSONResponse_on_success(self):
        # act
        result: JSONResponse = bank_card_service.delete(id='000ebdf0-220e-4765-86f9-4cc71265abcd', user=FAKE_USER)

        # assert
        self.assertIsInstance(result, JSONResponse)
        self.assertEqual(result.status_code, 200)

    @patch("services.bank_card_service.get_cards_by_user", Mock(return_value=LIST_FAKE_CARDS))
    @patch("services.bank_card_service.exists", Mock(return_value=FAKE_CARD))
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "one", Mock(return_value=BankCardsDB))
    @patch.object(Session, "delete", Mock(return_value=1))
    @patch.object(Session, "commit", Mock(return_value="ok"))
    def test_delete_return_JSONResponse_statusCode403_on_success(self):
        # act
        result: JSONResponse = bank_card_service.delete(id='000ebdf0-220e-4765-86f9-4cc71265abcd', user=FAKE_USER)

        # assert
        self.assertIsInstance(result, JSONResponse)
        self.assertEqual(result.status_code, 403)