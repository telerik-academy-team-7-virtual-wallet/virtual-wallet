import unittest
from unittest.mock import Mock, patch
from fastapi import Response
from fastapi.responses import JSONResponse
from services import contact_service
from sqlmodel import create_engine, Session
from models.transaction_model import Transactions
from models.contacts_model import Contacts, AddUserInContact_list
from models.user_model import UsersDB


fake_engine = create_engine(f"sqlite:///fake_file_name", echo=True)


FAKE_CONTACT = Contacts(id=1,
                         users_id='655d6703-749c-40c4-9437-e9418486dd5c',
                         users_id1='33bc6d1e-9cc2-4578-baea-52db642d7d5d')
FAKE_CONTACT_NAMES = ['PESHO', 'GOSHO']
FAKE_CONTACT_LIST = [Contacts(id=1,
                              users_id='655d6703-749c-40c4-9437-e9418486dd5c',
                              users_id1='155d6703-749c-40c4-9437-e9418486dd5b'),
                              Contacts(id=1,
                                       users_id='115d6703-749c-40c4-9437-e9418486dd5c',
                                       users_id1='225d6703-749c-40c4-9437-e9418486dd5b')]
FAKE_USER_ID = '33bc6d1e-9cc2-4578-baea-52db642d7d5d'
FAKE_CONTACT_IDS = ['225d6703-749c-40c4-9437-e9418486dd5b',
                    '225d6703-749c-40c4-9437-e9418486dd5b']
FAKE_USERNAME = 'Pesho'
FAKE_ADDED_CONTACT = AddUserInContact_list(username='Gosho')


class FakeResult:
    val = []

    @classmethod
    def all(cls):
        return cls.val

    @classmethod
    def first(cls):
        return cls.val
    
    @classmethod
    def execute(cls):
        return cls.val

    @classmethod
    def fetchall(cls):
        return cls.val



class FakeQuery():
    
    def query():
        pass

    def where():
        pass
    
    def filter_by():
        pass
    
    def delete():
        pass

    def filter():
        pass

    def one():
        pass



class ContactService_Should(unittest.TestCase):
    
    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "first", Mock(return_value=FAKE_USERNAME))
    @patch.object(Session, "add", Mock(return_value="ok"))
    @patch.object(Session, "commit", Mock(return_value="ok"))
    @patch.object(Session, "refresh", Mock(return_value=FAKE_CONTACT))
    @patch("services.category_service.ENGINE", Mock(return_value=fake_engine))
    def test_add_friend_in_contact_list(self):
        result: Contacts = contact_service.add_friend_in_contacts(FAKE_USER_ID, FAKE_ADDED_CONTACT)
        self.assertIsInstance(result, Contacts)
        


    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "all", Mock(return_value=FAKE_CONTACT_NAMES))
    @patch("services.contact_service.ENGINE", Mock(return_value=fake_engine))
    def test_get_all_usernames_return_lst_if_exists(self):
        result: list[UsersDB.username] = contact_service.get_all_usernames()
        self.assertEqual(len(result), 2)



    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "all", Mock(return_value=[]))
    @patch("services.contact_service.ENGINE", Mock(return_value=fake_engine))
    def test_get_all_usernames_return_empty_list_if_users_not_exist(self):
        result: list[UsersDB.username] = contact_service.get_all_usernames()
        self.assertEqual(result, [])



    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "all", Mock(return_value=FAKE_CONTACT_IDS))
    @patch("services.contact_service.ENGINE", Mock(return_value=fake_engine))
    def test_get_all_contact_ids_for_auth_user_return_lst_if_exist(self):
        result: list[Contacts.users_id1] = contact_service.get_all_contact_ids_for_auth_user(FAKE_USER_ID)
        self.assertEqual(len(result), 2)
       


    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "all", Mock(return_value=[]))
    @patch("services.contact_service.ENGINE", Mock(return_value=fake_engine))
    def test_get_all_contacts_ids_for_auth_user_return_empty_list_if_no_contact(self):
        result: list[Contacts.users_id1] = contact_service.get_all_contact_ids_for_auth_user(FAKE_USER_ID)
        self.assertEqual(result, [])
           


    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "all", Mock(return_value=FAKE_CONTACT_IDS))
    @patch("services.contact_service.ENGINE", Mock(return_value=fake_engine))
    def test_get_all_user_ids_return_lst_of_ids_if_users_exist(self):
        result: list[UsersDB.id] = contact_service.get_all_user_ids()
        self.assertEqual(len(result), 2)



    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "all", Mock(return_value=[]))
    @patch("services.contact_service.ENGINE", Mock(return_value=fake_engine))
    def test_get_all_user_ids_return_empty_list_if_users_not_exist(self):
        result: list[UsersDB.id] = contact_service.get_all_user_ids()
        self.assertEqual(result, [])  



    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "all", Mock(return_value=FAKE_CONTACT_IDS))
    @patch("services.contact_service.ENGINE", Mock(return_value=fake_engine))
    def test_get_all_reciever_ids_for_current_user_return_list(self):
        result: list[Transactions.receiver_id] = contact_service.get_all_reciever_ids_for_current_user(FAKE_USER_ID)
        self.assertEqual(len(result), 2)



    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "all", Mock(return_value=[]))
    @patch("services.contact_service.ENGINE", Mock(return_value=fake_engine))
    def test_get_all_reciever_ids_for_current_user_return_empty_list_if_no_transactions(self):
        result: list[Transactions.receiver_id] = contact_service.get_all_reciever_ids_for_current_user(FAKE_USER_ID)
        self.assertEqual(result, [])       



    @patch.object(Session, "execute", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "fetchall", Mock(return_value=FAKE_CONTACT_NAMES))
    @patch("services.contact_service.ENGINE", Mock(return_value=fake_engine))
    def test_get_all_usernames_from_user_ids_return_list_from_list_ids(self):
        result: list[UsersDB.username] = contact_service.get_all_usernames_from_user_ids(FAKE_CONTACT_IDS)
        self.assertEqual(len(result), 2)       



    @patch.object(Session, "execute", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "fetchall", Mock(return_value=[]))
    @patch("services.contact_service.ENGINE", Mock(return_value=fake_engine))
    def test_get_all_usernames_from_user_ids_return_empty_list(self):
        result: list[UsersDB.username] = contact_service.get_all_usernames_from_user_ids([])
        self.assertEqual(result, [])  



    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "first", Mock(return_value=FAKE_USER_ID))
    @patch("services.contact_service.ENGINE", Mock(return_value=fake_engine))
    def test_get_friend_id(self):
        result: UsersDB.id = contact_service.get_friend_id(FAKE_USERNAME)
        self.assertEqual(result, FAKE_CONTACT.users_id1)
        


    @patch.object(Session,"query", Mock(return_value=FakeQuery()))
    @patch.object(FakeQuery,"where", Mock(return_value=FakeQuery()))
    @patch.object(FakeQuery,"delete", Mock(return_value=1))
    @patch.object(Session,"commit", Mock(return_value="ok"))
    @patch("services.category_service.ENGINE", Mock(return_value=fake_engine))
    def test_delete_username_from_contact_list(self):
        result: Contacts = contact_service.delete_username_from_contact_list(id=FAKE_USER_ID)
        self.assertIsInstance(result, JSONResponse)
        self.assertEqual(result.status_code, 200)
